variable "namespace" {
    type        = string
    default     = "anwar"
    description = "Project namespace used for unique naming resource"
}

variable "region" {
    type        = string
    default     = "us-east-1"
    description = "description"
}
