output "public_connection_string" {
    description = "Copy/Paste Private Key to Public Instances"
    value   = "ssh -i ${module.ssh-key.key_name}.pem ec2-user@${module.ec2.public_ip}"
}

output "private_connection_string" {
    description = "Copy/Paste Private to Private Instances"
    value   = "ssh -i ${module.ssh-key.key_name}.pem ec2-user@${module.ec2.private_ip}"
}