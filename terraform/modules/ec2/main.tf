data "aws_ami" "amazon-linux-2" {
    most_recent = true
    owners  = ["amazon"]

    filter {
        name    = "name"
        values  = ["amzn2-ami-hvm*"]
    }
}

resource "aws_instance" "ec2_public" {
    ami = data.aws_ami.amazon-linux-2.id
    associate_public_ip_address   = true
    instance_type   = "t3.micro"
    key_name    = var.key_name
    subnet_id   = var.vpc.public_subnets[0]
    vpc_security_group_ids  = [var.sg_pub_id]

    user_data = <<-EOF
        echo "### Install Docker ###"
        set -ex
        sudo yum update -y
        sudo amazon-linux-extras install docker -y
        sudo service docker start
        sudo usermod -a -G docker ec2-user

        echo "### Install Docker Compose ###"
        sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
    EOF

    tags = {
        "name" = "${var.namespace}-EC2-Public"
    }

    # provisioner "file" {
    #     source  = "./anwar-key.pem"
    #     destination = "/home/ec2-user/${var.key_name}.pem"

    #     connection {
    #         type    = "ssh"
    #         user    = "ec2-user"
    #         private_key = file("${var.key_name}.pem")
    #         host    = self.public_ip
    #     }
    # }

    # provisioner "remote-exec" {
    #     inline  = ["chmod 400 ~/${var.key_name}.pem"]

    #     connection {
    #         type    = "ssh"
    #         user    = "ec2-user"
    #         private_key = file("${var.key_name}.pem")
    #         host    = self.public_ip
    #     }
    # }
}


resource "aws_instance" "ec2_private" {
    ami = data.aws_ami.amazon-linux-2.id
    associate_public_ip_address   = false
    instance_type   = "t3.micro"
    key_name    = var.key_name
    subnet_id   = var.vpc.private_subnets[1]
    vpc_security_group_ids  = [var.sg_priv_id]

    user_data = <<-EOF
        echo "### Install Docker ###"
        set -ex
        sudo yum update -y
        sudo amazon-linux-extras install docker -y
        sudo service docker start
        sudo usermod -a -G docker ec2-user

        echo "### Install Docker Compose ###"
        sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
    EOF

    tags = {
        "name" = "${var.namespace}-EC2-Private"
    }
}