data "aws_availability_zones" "available" {}

module "vpc" {
    source = "terraform-aws-modules/vpc/aws"
    name    = "${var.namespace}"
    cidr    = "10.0.0.0/16"
    azs     = data.aws_availability_zones.available.names
    private_subnets  = ["10.0.1.0/24","10.0.2.0/24"]
    public_subnets  = ["10.0.3.0/24","10.0.4.0/24"]

    create_database_subnet_group = true
    enable_nat_gateway = true
    single_nat_gateway = true
}

# Create Security for for any ec2
resource "aws_security_group" "public_sg" {
    name = "${var.namespace}-public_sg"
    description = "Security Group for Public EC2"
    vpc_id  = module.vpc.vpc_id

    ingress {
        description = "Allow SSH from the internet"
        from_port   = 22
        to_port = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "Allow HTTPS Connections"
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "Allow Database for Public Connections"
        from_port   = 3306
        to_port     = 3306
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "${var.namespace}-public_sg"
    }
}


# Create Security for Private EC2 server
resource "aws_security_group" "private_sg" {
    name = "${var.namespace}-private_sg"
    description = "Security Group for Public EC2"
    vpc_id  = module.vpc.vpc_id

    ingress {
        description = "Allow SSH only from the public srv"
        from_port   = 22
        to_port = 22
        protocol    = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }

    ingress {
        description = "Allow Database Connections"
        from_port   = 3306
        to_port     = 3306
        protocol    = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "${var.namespace}-private_sg"
    }
}